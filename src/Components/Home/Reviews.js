import { useState } from "react";
import review1 from "../images/review1.png";
import review2 from "../images/review2.png";
import review3 from "../images/review3.png";
import me from "../images/me.svg";
import ve from "../images/ve.svg";
import ReactPaginate from "react-paginate";


const Reviews = () => {
  const [Reviews] = useState([
    {
      id: 1,
      location: "Palo Alto, CA",
      name: "Hellen Jummy",
      info: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vitae in donec in nisi vitae. Vestibulum pellentesque eget laoreet adipiscing",
      image: review1,
      flag: me,
    },

    {
      id: 2,
      location: "Palo Alto, CA",
      name: "Isaac Oluwatemilorun",
      info: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vitae in donec in nisi vitae. Vestibulum pellentesque eget laoreet adipiscing",
      image: review2,
      flag: ve,
    },

    {
      id: 3,
      location: "Palo Alto, CA",
      name: "Hellen Jummy",
      info: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vitae in donec in nisi vitae. Vestibulum pellentesque eget laoreet adipiscing",
      image: review3,
      flag: me,
    },

   
  ]);
  const PER_PAGE =3 ;
  const [currentPage, setCurrentPage] = useState(0);

  function handlePageClick({ selected: selectedPage }) {
    setCurrentPage(selectedPage);
  }

  const offset = currentPage * PER_PAGE;

  const currentPageData = Reviews.slice(offset, offset + PER_PAGE).map(
    (review) => (
      <div
        class="card col col-xs-12 review-cards"
        style={{ marginBottom: "10px" }}
      >
        <img src={review.image} class="avatar" alt="..." />
        <span class="review-name">{review.name}</span>
        <span class="review-location">
          <img
            src={review.flag}
            alt="flag"
            style={{ marginRight: "10px" }}
          ></img>
          {review.location}
        </span>
        <div class="card-body">
          <p class="review-content">{review.info}</p>
        </div>
      </div>
    )
  );

 
  const pageCount = Math.ceil(Reviews.length / PER_PAGE);

  return (
    <div className=" container">
      <div class="row">
        <div class="col-sm-6 col-md-6 upcoming-show-heading">Reviews</div>
        
        <div class="col-sm-6 col-md-6 pagination-box">
          <ReactPaginate
            previousLabel={"←"}
            nextLabel={"→"}
            previousClassName={"rami"}
            pageCount={pageCount}
            onPageChange={handlePageClick}
            containerClassName={"pagination"}
            nextClassName={"rami"}
            disabledClassName={"pagination__link--disabled"}
            activeClassName={"pagination__link--active"}
          />
          
          <div className="show-page-count">
            {currentPage + 1}
            <spam style={{ color: "grey" }}>/{pageCount}</spam>
          </div>
        </div>
      </div>

      {/* Cards */}
      <div class="">
        <div class="row">{currentPageData}</div>
      </div>
    </div>
  );
};

export default Reviews;
